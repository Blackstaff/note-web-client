$(document).ready(function() {
        $('#get_notes').click(function() {
            getNotes('/get_notes');
        });

        $('#create_note').click(function() {
                $.get('http://len.iem.pw.edu.pl:4577/create_note')
                    .success(function(e) {
                        $('#body').html(e);
                    })
                    .error(function(jqXHR, textStatus, errorThrown) {
                        $('#body').html(jqXHR.status);
                    });
        });

        $('#get_categories').click(showCategoryList);
        $('#add_category').click(function() { 
                $.get('http://len.iem.pw.edu.pl:4577/add_category', function(e) {
                        $('#body').html(e);
                });
        });
        $('#edit_note').click(function() {
                var noteId = $(this).attr('val');
                $.get('http://len.iem.pw.edu.pl:4577/edit_note/' + noteId, function(e) {
                        $('#body').html(e);
                });
        });

        $('#get_tags').click(showTagList);
});

function getNotes(url) {
                $.get(url)
                    .success(function(e) {
                        $('#body').html(e);

                        $('.read-note').click(function() {
                                var noteId = $(this).attr('value');
                                $.get('http://len.iem.pw.edu.pl:4577/read_note/' + noteId, function(e) {
                                    $('#body').html(e);
                                });
                        });

                        $('.edit-note').click(function() {
                                var noteId = $(this).attr('value');
                                $.get('http://len.iem.pw.edu.pl:4577/edit_note/' + noteId, function(e) {
                                    $('#body').html(e);
                                });
                        });

                        $('.delete-note').click(function() {
                                var noteId = $(this).attr('value');
                                $.get('http://len.iem.pw.edu.pl:4577/delete_note/' + noteId, function(e) {
                                    $('#body').html(e);
                                });
                        });
                       /* $('p.message').click(function() {
                                inbox = false;
                                var messageId = $(this).attr('id')
                                $.get('http://len.iem.pw.edu.pl/~czarnem1/apps/mail/read_message/' + messageId, function(e) {
                                        $('#body').html(e);
                                        $('button.delete_message').click(function() {
                                                var messageId = $(this).attr('id')
                                                $.get('http://len.iem.pw.edu.pl/~czarnem1/apps/mail/delete_message/'
                                                         + messageId, function(e) {
                                                        $('#body').html('<p>Wiadomość usunięta</p>');
                                                });
                                        });
                                });
                        });*/
                })
                .error(function(jqXHR, textStatus, errorThrown) {
                    $('#body').html(jqXHR.status);
                });
}

function showCategoryList() {
        $.get('http://len.iem.pw.edu.pl:4577/get_categories', function(e) {
            $('#body').html(e);
            $('.btn-danger').click(function() {
                var categoryId = $(this).attr('val')
                $.get('http://len.iem.pw.edu.pl:4577/delete_category/' + categoryId, function(e) {
                    showCategoryList(); 
                });
            }); 

            $('.show-notes').click(function() {
                var categoryId = $(this).attr('val');
                url = "/get_category_notes/" + categoryId;
                getNotes(url)
            });
 
            $('.edit').click(function() {
                var categoryId = $(this).attr('val');
                url = "/edit_category/" + categoryId;
                $.get('http://len.iem.pw.edu.pl:4577/edit_category/' + categoryId, function(e) {
                        $('#body').html(e);
                });
            }); 
        });
}

function showTagList() {
        $.get('http://len.iem.pw.edu.pl:4577/get_tags', function(e) {
            $('#body').html(e);
            $('.show-notes').click(function() {
                var tag = $(this).attr('val');
                url = "/get_tag_notes/" + tag;
                getNotes(url)
            }); 
        });
}
