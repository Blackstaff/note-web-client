require 'sinatra'
require 'unirest'


configure do
    set :bind, '0.0.0.0'
    set :port, 4577
    set :show_exceptions, false
end

get '/' do
    erb :index
end

get '/get_notes' do
    url = 'http://len.iem.pw.edu.pl/~czarnem1/apps/webservice/note'
    response = Unirest.get(url)
    @note_list = response.body

    url = 'http://len.iem.pw.edu.pl/~czarnem1/apps/webservice/category'
    response = Unirest.get(url)
    if response.code != 200
        status response.code
    end
    @category_list = response.body

    erb :note_list
end

get '/read_note/:noteId' do
    url = 'http://len.iem.pw.edu.pl/~czarnem1/apps/webservice/note/' + params[:noteId]
    response = Unirest.get(url)
    check_response(response)
    @note = response.body
    erb :note
end

get '/create_note' do
    url = 'http://len.iem.pw.edu.pl/~czarnem1/apps/webservice/category'
    response = Unirest.get(url)
    @category_list = response.body
    
    erb :note_form
end

post '/create_note' do
    title = params[:title]
    category = params[:category]
    tags = params[:tags]
    content = params[:content]
    
    note_hash = {
        'title'       => title,
        'category_id' => category.to_i,
        'tags'        => tags,
        'content'     => content
    }

    url = 'http://len.iem.pw.edu.pl/~czarnem1/apps/webservice/note'
    response = Unirest.post(url, headers:{ "Accept" => "application/json"}, parameters: note_hash.to_json)
    check_response(response)

    @message = response.body['message']
    erb :server_message
end

get '/edit_note/:noteId' do
    url = 'http://len.iem.pw.edu.pl/~czarnem1/apps/webservice/note/' + params[:noteId]
    response = Unirest.get(url)
    @note = response.body

    url = 'http://len.iem.pw.edu.pl/~czarnem1/apps/webservice/category'
    response = Unirest.get(url)
    check_response(response)

    @category_list = response.body
    erb :edit_note
end

post '/edit_note/:noteId' do
    title = params[:title]
    category = params[:category]
    tags = params[:tags]
    content = params[:content]
    
    note_hash = {
        'title'       => title,
        'category_id' => category.to_i,
        'tags'        => tags,
        'content'     => content
    }

    url = 'http://len.iem.pw.edu.pl/~czarnem1/apps/webservice/note/' + params[:noteId]
    response = Unirest.put(url, headers:{ "Accept" => "application/json"}, parameters: note_hash.to_json)
    check_response(response)
    @message = response.body['message']
    erb :server_message
end

get '/delete_note/:noteId' do
    url = 'http://len.iem.pw.edu.pl/~czarnem1/apps/webservice/note/' + params[:noteId]
    response = Unirest.delete(url)
    @message = response.body['message']
    erb :server_message
end

get '/add_category' do
    @action = :create
    erb :category_form
end

post '/add_category' do
    category_name = params[:category_name]
    url = 'http://len.iem.pw.edu.pl/~czarnem1/apps/webservice/category'
    response = Unirest.post(url, headers:{ "Accept" => "application/json"},
        parameters: {"category_name" => category_name}.to_json)
    check_response(response)
    @message = response.body['message']
    erb :server_message
end

get '/edit_category/:categoryId' do
    @category_id = params[:categoryId]
    @action = :edit
    erb :category_form
end

post '/edit_category/:categoryId' do
    category_name = params[:category_name]
    url = 'http://len.iem.pw.edu.pl/~czarnem1/apps/webservice/category/' + params[:categoryId]
    response = Unirest.put(url, headers:{ "Accept" => "application/json"},
        parameters: {"category_name" => category_name}.to_json)
    check_response(response)
    @message = response.body['message']
    erb :server_message
end

get '/delete_category/:categoryId' do
    url = 'http://len.iem.pw.edu.pl/~czarnem1/apps/webservice/category/' + params[:categoryId]
    response = Unirest.delete(url)
    check_response(response)
    @message = response.body['message']
    erb :server_message
end

get '/get_category_notes/:categoryId'do
    url = 'http://len.iem.pw.edu.pl/~czarnem1/apps/webservice/category/' + params[:categoryId] + '/notes'
    response = Unirest.get(url)
    check_response(response)
    @note_list = response.body

    url = 'http://len.iem.pw.edu.pl/~czarnem1/apps/webservice/category'
    response = Unirest.get(url)
    @category_list = response.body
    erb :note_list
end

get '/get_categories' do
    url = 'http://len.iem.pw.edu.pl/~czarnem1/apps/webservice/category'
    response = Unirest.get(url)
    check_response(response)
    @category_list = response.body
    erb :category_list
end

get '/get_tags' do
    url = 'http://len.iem.pw.edu.pl/~czarnem1/apps/webservice/tag'
    response = Unirest.get(url)
    check_response(response)
    @tag_list = response.body['tag_names']
    erb :tag_list
end

get '/get_tag_notes/:tag'do
    url = 'http://len.iem.pw.edu.pl/~czarnem1/apps/webservice/tag/' + params[:tag] + '/notes'
    response = Unirest.get(url)
    check_response(response)
    @note_list = response.body

    url = 'http://len.iem.pw.edu.pl/~czarnem1/apps/webservice/category'
    response = Unirest.get(url)
    check_response(response)
    @category_list = response.body

    erb :note_list
end

not_found do
    status 404
    erb :not_found
end

error do
    erb :error 
end

def check_response(response)
    if response.code != 200
        @response_code = response.code
        status @response_code
        if response.code == 403
            erb :forbidden
        end
    end
end
